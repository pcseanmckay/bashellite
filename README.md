# Bashellite

## Purpose

Bashellite is a module, generic, mirroring tool used to mirror as many different repository types as possible. It has a pluggable architecture that lets you write wrapper functions that call the "providers" that you would normally use to mirror a specific type of repository. As an example, it uses the following repository types using the following protocols and tools. Please note that this is not an exhaustive list.

- rsyncd mirrors via the rsync protocol using the `rsync` provider
- http/https mirrors via the http/https protocols using the `wget` provider
- Debian mirrors via the http/https protocols using the `apt-mirror` provider

If you find yourself trying to mirror several different types of repositories (e.g., rpm, deb, python, containers, etc.),
and you can not find a single tool that does this effectively, Bashellite might be what you're looking for.

## Features

Bashellite includes many of the same configuration options used by the provider utilities that it calls to perform mirroring. Bashellite simply gives these tools some sensible default flags/options, and a standardized, user-friendly, highly-configurable interface. Many of the options you would normally pass over the command-line are either embedded in the script for you (if they are not something you typically change) or they have been farmed out to configuration files that you then populate with data (if they are something you typically change). This alleviates the need to remember cumbersome command-line flags for each of the underlying tools. Much of the heavy lifting is taken care of for you.

As an example, Bashellite does syntax checking for you, and prints useful error messages when you make a mistake via it's robust, highly-greppable system of logs. It also has a dry-run mode that shows you exactly what it would do during a real run. However, it should be noted that since some of the underlying provider tools do not natively provide a dry-run mode themselves, Bashellite does the best it can to simulate a real run without downloading excess data or making any significant changes to the filesystem.

## Usage

```
         Usage: bashellite
                [-m mirror_top-level_directory]
                [-c configuration_top-level_directory]
                [-p provider_top-level_directory]
                [-h]
                [-d]
                [-r repository_name] | [-a]

                Optional Parameter(s):
                -m:  Sets a temporary disk mirror top-level directory.
                     Only absolute (full) paths are accepted!
                -c:  Sets a temporary configuration top-level directory.
                     Only absolute (full) paths are accepted!
                -p:  Sets a temporary provider top-level directory.
                     Only absolute (full) paths are accepted!
                -h:  Prints this usage message.
                -d:  Dry-run mode. Pulls down a listing of the files and
                     directories it would download, and then exits.
                -r:  The repo name to sync.
                -a:  Mutually exclusive with -r option; sync all repos.

                Note: Repositories can be grouped by naming them with "__";
                      for example, repo: "images__linux" becomes images/linux
                      inside of the mirror directory passed via the "-m" flag.
```

## Quickstart (with no customization)

### Prerequisites

Using the vendor-recommended method for your distribution, installed the following applications:

- vagrant
- git

### Provision a Bashellite Host

First, git clone a copy of the bashellite repository, then change directories into it.

```
git clone https://gitlab.com/dreamer-labs/bashellite/bashellite.git;
cd bashellite;
```

Next, find the src/ directory, and select one of the desired environment by changing directories into the environment's subdirectory.

```
cd src/host-specific/vagrant/centos8/podman/;
```

Once in the desired directory, run the vagrant command required to bootstrap the Bashellite Host; once up, login.

Note: This `Vagrantfile` makes some opinionated decisions about how you want this VM's networking configured.
Please take a look at the file, and use the Vagrant documentation to adjust the networking as you see fit.
The way the networking is configured is largely irrelevant so long as you can ssh in (with `vagrant ssh`)
and the VM has access to the internet.

```
vagrant up bh-centos8-podman && vagrant ssh bh-centos8-podman
```

At first, you will be logged in as the `vagrant` user.
This user has sudo access, but unless you're doing development on Bashellite, you will not use this frequently.
Login as the `bashellite` user and change directories to the bashellite user's home directory.

```
sudo su bashellite;
cd;
```

For here, execute the bashellite bootstrap script.
This script must only be executed as the bashellite user,
because it will create a series of containers that are only executable by that specific non-root user.

Note: this script requires you to pass a container builder name to it. In this example, `buildah` will be used.
In other environments, this might be `docker` or something else.

```
/opt/bashellite/bootstrap-service.sh buildah;

```

This script will build the base images containing the bashellite source application.
Each provider uses one of these base images, installing additional system packages and source code on top of it.
Once the base images are complete, the script builds the provider images.

Once the base and provider images are built and tagged, the `sync-repo.sh` script can be run as a test.
This script accepts the repo name as its one and only parameter. This example uses a repository that ships with Bashellite.
The repository is configured under `/etc/bashellite/repos.conf.d/test/` on the Bashellite Host and is called `test`.

```
/opt/bashellite/sync-repo.sh test;
```

This script will parse the configured provider that the repo requires for syncing. It will then execute the associated provider image.
The container manager will mount several directories on the Bashellite Host so that it can write to them and/or read from them, as appropriate. These directories include:

- /etc/bashellite/ (to read repo configuration files)
- /opt/bashellite/ (to read provider wrapper scripts used to invoke providers installed in the provider containers)
- /var/log/bashellite/ (to write log data)
- /var/www/bashellite/mirror/ (to write repository data that is synced)

This script is intended for production use only. As a result, it does not include features for overriding any of these locations for alternate locations on the filesystem.

When the test repo sync executes, it does not download any data. Instead, it prints to screen several variables and their values, and writes this same information to a log file.

## Adding Custom Repositories

After completing the quickstart installation process, in order to download real data, the configuration directory must be populated with repo metadata other than the metadata that ships with bashellite (i.e. the "test" repository metadata). The default location of the configuration directory is:

- /etc/bashellite/repos.conf.d/

If you would like to borrow some pre-built, valid, repo-config metadata, please visit this repository:

- https://gitlab.com/dreamer-labs/bashellite/bashellite-configs

Feel free to either clone the entire repository down to the Bashellite Host, or cherry-pick just a repo directory or two for testing.

### Understanding Repo Configuration Files

Each repository has two files used to configure how `bashellite` syncs that repo's data.

The first repo config file is the main repository configuration file. This file is called:

- repo.conf

Each main repo config file contains the following parameters:

- repo_url (location of the repository that will be synced from)
- repo_provider (name of the provider that will be used to perform the sync operation)

The format of the `repo_url` parameter will vary depending on the provider.
For example, if the provider uses the HTTP/HTTPS protocols, the URL will start with "https://" or "http://";
however, if it uses the rsync protocol, the URL will start with "rsync://".
See the documentation for each provider or example bashellite configuration files for more information.

The `repo_provider` parameter will match one of the providers listed in `/opt/bashellite/providers.d/`.

The second repo config file is the provider's configuration file for the repository.

Since bashellite is essentially just a framework for wrapping various sync operation providers,
each provider that bashellite wraps, already has its own native configuration file.
Rather than provide another abstraction layer, bashellite uses this native format for each provider.

These files typically contain information like package denylists and/or package allowlists.
A sample config file for each provider's config file is available in `/opt/bashellite/provider.d/${provider}`.

## Understanding Bashellite Logging

Bashellite implements its own robust logging mechanism designed to capture both events and errors in a grep-friendly manner. Errors generated during the sync process are written to: `/var/log/bashellite/`

### Log File Naming Schema

Each repository gets two logs for each and every run Bashellite attempts. One log is the "event" log that documents STDOUT messages from the script and providers. The other log is an "error" log that records STDERR. The naming schema is as follows:

- `/var/log/bashellite/<repo_name>.<YYMMDD>.<run_id>.error.log`
- `/var/log/bashellite/<repo_name>.<YYMMDD>.<run_id>.event.log`

### run_id

The "run_id" is a number generated by the `date` command, and is part of each log name. It ensures that all logs from the same run have matching ids, while ensuring that runs for the same repo, on the same day, but from different runs, have different ids, and therefore slightly different log names. The run_ids are arranged in numerical and chronological order, so a lower numbered run_id will indicate that a run occurred earlier in the day when compared to another run from that same day with a higher run_id.

### Message levels

The script prints messages at three levels, and only has one level of verbosity. The message levels are `INFO`, `WARN`, and `FAIL`. `INFO` messages are strictly informational, and are provided for debugging and informational purposes; these are logged in the event log and sent to STDOUT on the terminal. `WARN` messages indicate a problem and are logged to the error log; `WARN` messages may or may not be followed by a `FAIL` message. If they are not, the script continues to execute. `FAIL` messages indicate an issue that is severe enough to warrant stopping execution of the script, and whenever a fail message is invoked, the script always returns a non-zero exit code. `FAIL` messages are logged in the error log, along with `WARN` messages.

### Log entry numbers

As you are watching output scroll by on the terminal, or reading the logs, you may notice a number on the left-hand side of some lines. These are log entry numbers. They are generated with the `date` command and are in both numerical and chronological order. the format is: HHMMSSNN, where "NN" is the first two digits of the nanosecond. This log entry number ensures that no two entries will have the same number, and even lets you compare across the same time each day if your grep command is written appropriately. These numbers also appear on terminal output so that you can quickly find this same error in the logs. It is written to both simultaneously.

### Unlogged Errors

On rare occasions, you may run into instances where Bashellite fails, but the error is not reported in a log. This is because Bashellite performs some preparatory administrative checks prior to logging being fully setup. If you come across an unlogged failure when invoking Bashellite from a cron job or other non-interactive method, try running it manually from the command-line under similar conditions (i.e. same user, etc.). This test may generate an error message that is printed to STDOUT, but not logged to `/var/log/bashellite`. If you still cannot figure out why the script is failing, there is a debug option in the script that can be toggled on/off by manually editing the Bashellite script itself. It is clearly marked and located towards the top of the file; this should only be used as a last resort due to it's verbosity.
