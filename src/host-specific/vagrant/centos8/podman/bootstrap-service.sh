#!/usr/bin/env bash

# Name of the program
PROGNAME="bashellite";

# Location of the program's source code and binaries (excluding configs)
OPTDIR="/opt/${PROGNAME}";

# Location of the source code used to build base images
BASESDIR="${OPTDIR}/bases.d";

# Location of the source code used to build provider images
PROVIDERSDIR="${OPTDIR}/providers.d";

case ${1} in
  docker)
    build_cmd="docker";
    build_subcmd="build";
    ;;
  buildah|podman)
    build_cmd="buildah";
    build_subcmd="bud";
    ;;
  *)
    echo -e "\n[FAIL] No image builder name provided via first command-line positional argument." 1>&2; exit 1;
    ;;
esac;

for base_dir in ${BASESDIR}/*; do
  base="${base_dir##*/}";
  if [[ -d "${base_dir}" ]]; then
    if [[ -f "${base_dir}/Dockerfile" ]]; then
      echo -e "\n[INFO] Dockerfile found for base: ${base}" && \
      cd "${base_dir}/" && \
      { echo -e "\n[INFO] Building ${PROGNAME} base container image with: ${build_cmd}" \
        && "${build_cmd}" "${build_subcmd}" -f "Dockerfile" --build-arg UID=$(id -u) -t "${PROGNAME}-base-${base}:latest" "/opt/${PROGNAME}/bases.d/.data/" \
        && echo -e "\n[INFO] Successfully built ${PROGNAME} base container image (${base})."; } \
      || { echo -e "\n[FAIL] Failed to build ${PROGNAME}-base-${base} container image." 1>&2; exit 1; };
    else
      echo -e "\n[WARN] No Dockerfile found for base: ${base}" 1>&2;
    fi;
  fi;
done;

for provider_dir in ${PROVIDERSDIR}/*; do
  provider="${provider_dir##*/}";
  if [[ -d "${provider_dir}" ]]; then
    if [[ -f "${provider_dir}/Dockerfile" ]]; then
      echo -e "\n[INFO] Dockerfile found for provider: ${provider}" && \
      cd "${provider_dir}/" && \
      { echo -e "\n[INFO] Building ${PROGNAME}-provider-${provider} container image with: ${build_cmd}" \
        && "${build_cmd}" "${build_subcmd}" -f "Dockerfile" -t "${PROGNAME}-provider-${provider}:latest" "." \
        && echo -e "\n[INFO] Successfully build ${PROGNAME} provider container image (${provider})."; } \
      || { echo -e "\n[FAIL] Failed to build ${PROGNAME}-provider-${provider} container image." 1>&2; exit 1; };
    else
      echo -e "\n[WARN] No Dockerfile found for provider: ${provider}" 1>&2;
    fi;
  fi;
done;


