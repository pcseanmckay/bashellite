#!/usr/bin/env bash

# SYS_DEPS are the system packages required on the bashellite host
# This variable is used to install packages via dnf
SYS_DEPS="vim python3-pip"

# PROGNAME is the name of the program
# This variable is used to 
PROGNAME="bashellite"

# USERNAME is the name of the service account user
# This variable is used to create the service account user and set perms
USERNAME="${PROGNAME}"

# HOMEDIR is the location of the service account user's home directory
# This variable is used to set the home directory at service account user creation
HOMEDIR="/home/${USERNAME}"

# INSTALLDIR is a generic installation file staging location for source code and binaries on the system
# This variable is used to create the installation directory
INSTALLDIR="${HOMEDIR}/.install"

# WORKDIR is the location where bashellite installation files in particular are stored
# This variable is used to create the working directory
WORKDIR="${INSTALLDIR}/bashellite"

# STAGEDIR must be passed in as environment variable by parent process
# This variable is used to move files staged on the host into the install directory

# ETCDIR is a directory containing configuration files for bashellite
# This variable is used to create and reference the bashellite config directory
ETCDIR="/etc/${PROGNAME}"

# CONFDIR is a sub-directory containing configuration files for bashellite repos
# This variable is used to create and reference the bashellite repo config sub-directory
CONFDIR="${ETCDIR}/repos.conf.d"

# OPTDIR is a directory containing bashellite provider-system-related files
# This variable is used to create and reference the bashellite provider-system files
OPTDIR="/opt/${PROGNAME}"

# PROVIDERDIR is the sub-directory containing provider-specific directories
# This variable is used to create and reference the bashellite provider-specific files
PROVIDERDIR=${OPTDIR}/providers.d

# BASEDIR is the sub-directory containing base-image-specific directories
# This variable is used to create and reference the bashellite base-image-specific files
BASEDIR=${OPTDIR}/bases.d

# TESTDIR is the sub-directory containing test directories
# This variable is used to create and reference the bashellite test files
TESTDIR=${OPTDIR}/tests.d

# SRCDIR is the sub-directory containing bashellite source code
# This variable is used to create and reference the bashellite bashellite source code files
SRCDIR=${OPTDIR}/src

# LOGDIR is the directory containing bashellite log files
# This variable is used to create and reference the bashellite log directory
LOGDIR="/var/log/${PROGNAME}"

# MIRRORDIR is the directory containing the default bashellite mirror files directory
# This variable is used to create and reference the bashellite mirror directory
MIRRORDIR="/var/www/bashellite/mirror"

ls -hlaR; \
{ echo -e "\n[INFO] Setting up user (${USERNAME})."; \
  { adduser -d "${HOMEDIR}" -s "/bin/bash" "${USERNAME}" || true; }; } && \
{ echo -e "\n[INFO] Validating that data staging directory (${STAGEDIR}) exists."; \
  if [[ ! -d "${STAGEDIR}" ]]; then \
    echo -e "\n[FAIL] Stage directory (${STAGEDIR}) either undefined or not a directory; exiting."; \
    exit 1; \
  fi; } && \
{ echo -e "\n[INFO] Creating, populating, and setting up perms for working directory: ${WORKDIR}"; \
  { mkdir -p "${INSTALLDIR}" && \
    cp -fR "${STAGEDIR}" "${WORKDIR}" && \
    chown -R "${USERNAME}:${USERNAME}" "${HOMEDIR}" && \
    chmod 2770 "${HOMEDIR}" && \
    chmod 2770 "${INSTALLDIR}" && \
    chmod 2770 "${WORKDIR}";} || exit 1; } && \
{ echo -e "\n[INFO] Creating and setting up perms on default log directory: ${LOGDIR}"; \
  { mkdir -p "${LOGDIR}" && \
    chown -R "${USERNAME}:${USERNAME}" "${LOGDIR}" && \
    chmod 2750 "${LOGDIR}"; } || exit 1; } && \
{ echo -e "\n[INFO] Creating and setting up perms on default mirror directory: ${MIRRORDIR}"; \
  { mkdir -p "${MIRRORDIR}" && \
    chown "${USERNAME}:${USERNAME}" "${MIRRORDIR}" && \
    chmod 2750 "${MIRRORDIR}"; } || exit 1; } && \
{ echo -e "\n[INFO] Configuring default config file directory: ${CONFDIR}"; \
  { mkdir -p "${CONFDIR}" && \
    chmod 2750 "${ETCDIR}" && \
    chmod 2750 "${CONFDIR}" && \
    cp -fR "${WORKDIR}/src/common/etc/bashellite/repos.conf.d/test/" "${CONFDIR}/test/" && \
    install \
      -o root -g bashellite -m 0640 \
      -D "${WORKDIR}/src/common/etc/bashellite/bashellite.conf" "${ETCDIR}/bashellite.conf" && \
    chown -R "root:${USERNAME}" "${ETCDIR}"; } || exit 1; } && \
{ echo -e "\n[INFO] Creating and setting up perms on bashellite application directory."; \
  { mkdir -p "${OPTDIR}" && \
    chmod 2750 "${OPTDIR}"; } || exit 1; } && \
{ echo -e "\n[INFO] Installing bashellite source code."; \
  { install \
      -o "root" -g "bashellite" -m 0550 \
      -D "${WORKDIR}/src/host-specific/vagrant/centos8/podman/bootstrap-service.sh" "${OPTDIR}/bootstrap-service.sh" && \
    install \
      -o "root" -g "bashellite" -m 0550 \
      -D "${WORKDIR}/src/host-specific/vagrant/centos8/podman/sync-repo.sh" "${OPTDIR}/sync-repo.sh" && \
      cp -fR "${WORKDIR}/src/common/bases.d/" "${BASEDIR}/" && \
      cp -fR "${WORKDIR}/src/common/providers.d/" "${PROVIDERDIR}/" && \
      cp -fR "${WORKDIR}/src/common/tests.d/" "${TESTDIR}/" && \
    chown -R "root:${USERNAME}" "${OPTDIR}" && \
    systemctl daemon-reload; } || exit 1; } && \
{ echo -e "\n[INFO] Installing container-tools."; \
  dnf -y module enable container-tools && \
  dnf -y install criu && \
  dnf -y module disable container-tools && \
  dnf -y install 'dnf-command(copr)' && \
  dnf -y copr enable rhcontainerbot/container-selinux && \
  cd /etc/yum.repos.d/ && \
  wget https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable/CentOS_8/devel:kubic:libcontainers:stable.repo && \
  dnf -y install buildah podman skopeo || exit 1; } && \
{ echo -e "\n[INFO] Installing generic system deps."; \
  { for SYS_DEP in ${SYS_DEPS}; do \
      echo -e "\n[INFO] Installing sys dep: ${SYS_DEP}"; \
      dnf install -y --allowerasing "${SYS_DEP}"; \
    done; } || exit 1; } && \
{ echo -e "\n[INFO] Installing Ansible and its deps."; \
  { pip3 install -U ansible; } || exit 1; };
