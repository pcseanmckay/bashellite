#!/usr/bin/env bash

# shellcheck disable=SC1091

MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

oneTimeSetUp() {

  local lib_dir
  lib_dir=$(realpath "${MY_DIR}/../../bases.d/.data/usr/local/lib")

  # shellcheck source=usr/local/lib/util-libs.sh
  source ${lib_dir}/util-libs.sh;
}

testUtilDate() {
  rslt=$( date() {
    if [[ "$*" == *"--version"* ]]; then
      echo 'GNU date'
    else
      echo "2018-05-07"
    fi
  };
  utilDate;)

  assertTrue "utilDate failed to return successfully" $?

  # Test expected value
  assertSame "180507" "$rslt";

}



testUtilDateNotGNU() {
  # Moc out date
  rslt=$( date(){ echo "Not Date"; }; utilDate 2>&1;)
  assertFalse "Failed to detect Not GNU Date" $?
  assertSame "[FAIL] Dependency (date) not GNU version!" "$rslt"

}

testUtilDateNoOutput() {
  # Moc out date
  rslt=$( date() {
    if [[ "$*" == *"--version"* ]]; then
      echo "GNU date"
    else
      echo ''
    fi
  };
  utilDate 2>&1;
  )

  assertFalse "Date should fail but passed with no mocked date output" $?
  assertSame '[FAIL] Failed to set datestamp; ensure date supports "--iso-8601" flag!' "$rslt"
}

# shellcheck source=/dev/null
source "$(which shunit2)";
