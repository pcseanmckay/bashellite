#!/usr/bin/env bash

# shellcheck disable=SC2034,SC1091

MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

oneTimeSetUp() {
  local lib_dir
  lib_dir=$(realpath "${MY_DIR}/../../bases.d/.data/usr/local/lib")

  # shellcheck source=usr/local/lib/util-libs.sh
  source ${lib_dir}/util-libs.sh;
}

testUtilGNU_BadBin() {

  # Tests fail condition for if requested_function_input contains invalid characters
  rslt="$(utilGNU f00b@r! 2>&1)"
  assertFalse "utilGNU returned valid but we expected it to fail" $?
  assertSame "[FAIL] Requested GNU dependency or array name contains invalid characters; exiting." "$rslt";
}

testUtilGNU_SinglDep(){
  # Tests success condition for valid array names and single deps
  ( grep() { echo "This is the GNU version of grep"; }; utilGNU grep &>/dev/null; );
  assertTrue "Failed to detect GNU in mocked grep" $?;
}

testUtilGNU_ArraySingleDep() {
  ( grep() { echo "Yay GNU"; }; unittest_array=( grep ); utilGNU unittest_array &>/dev/null; )
  assertTrue "Failed to detect GNU in mocked grep with array" $?
}

testUtilGNU_NotGNU(){
  # Tests fail condition for if a dependency is not a GNU dep

  rslt=$( wrong_grep() { echo " x GNU This is not the FSF-released version of dep"; };
  utilGNU wrong_dep 2>&1 )

  assertFalse "utilGNU did not return an error on non GNU" $?

  assertSame "[FAIL] Dependency (wrong_dep) not GNU version!" "$rslt";

}

# shellcheck source=/dev/null
source "$(which shunit2)";
