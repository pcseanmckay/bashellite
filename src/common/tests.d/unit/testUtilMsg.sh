#!/usr/bin/env bash

# shellcheck disable=SC1091,SC2034

MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

oneTimeSetUp() {
  local lib_dir
  lib_dir=$(realpath "${MY_DIR}/../../bases.d/.data/usr/local/lib")

  # shellcheck source=usr/local/lib/util-libs.sh
  source ${lib_dir}/util-libs.sh;
}

testUtilMsg_ParamChecks() {
  rslt=$(utilMsg 1 2 2>&1);
  assertFalse $?
  # Tests minimum parameter failure message and return code output
  assertSame "[FAIL] Failed to pass in minimum number of required parameters to function (utilMsg); exiting." "${rslt}";

}

testUtilMsg_InputFail1(){
  rslt=$(utilMsg F00 1234 message);
  # Tests msg_type sanitizing failure message and return code output
  assertFalse $?
  assertSame "[FAIL] Requested message type (F00) passed to function (utilMsg) contains invalid characters; exiting." "${rslt}";

}

testUtilMsg_InputFail2() {
  rslt=$(utilMsg Fail 1234 message);
  assertFalse $?

}

testUtilMsg_InputFail3() {
  rslt=$(utilMsg \!\*\$F5 1234 message);
  assertFalse $?

}

testUtilMsg_InputPass1() {
  rslt=$(utilMsg INFO 1234 message);
  assertTrue $?

}

testUtilDeps_Timestamp1() {
  # Tests timestamp sanitizing failure message and return code output
  rslt=$(utilMsg INFO foob@r message);
  assertFalse $?
  assertSame "[FAIL] Requested timestamp (foob@r) passed to function (utilMsg) contains invalid characters; exiting." "${rslt}";
}


testUtilDeps_Timestamp2(){
  rslt=$(utilMsg INFO "my,big F00-barred.time_stamp" message)
  assertTrue $?

}

testUtilDeps_Timestamp3(){
  rslt=$(utilMsg INFO "Fri, 04 May 2018 07:26:36 -0500" message)
  assertTrue $?
}

testUtilDeps_Colors(){
  # Tests msg_type case statement logic
  assertSame "1234 green[INFO] message" "$(PROG_MSG_GREEN="green"; utilMsg INFO 1234 message 2>&1)";

  assertSame "1234 yellow[WARN] message" "$(PROG_MSG_YELLOW="yellow"; utilMsg WARN 1234 message 2>&1)";

  assertSame "1234 red[FAIL] message" "$(PROG_MSG_RED="red"; utilMsg FAIL 1234 message 2>&1)";

  assertSame "1234 blue[SKIP] message" "$(PROG_MSG_BLUE="blue"; utilMsg SKIP 1234 message 2>&1)";
}

testUtilDeps_NULL_as_message(){
  assertSame "[FAIL] The passed parameter (NULL) is not a valid message type; exiting." "$(utilMsg NULL 1234 message 2>&1)";
}

testUtilDeps_INFO(){
  # Tests Info messaging output and return code
  rslt=$(utilMsg INFO 1234 "message");
  assertTrue $?
  assertSame "1234 [INFO] message" "${rslt}";
}

testUtilDeps_WARN(){
  # Tests Warn messaging output and return code
  rslt=$(utilMsg WARN 1234 "message" 2>&1);
  assertTrue $?
  assertSame "1234 [WARN] message" "${rslt}";
}

testUtilDeps_FAIL(){
  # Tests Fail messaging output and return code
  rslt=$(utilMsg FAIL 1234 "message" 2>&1);
  assertFalse $?
  assertSame "1234 [FAIL] message" "${rslt}";
}

testUtilDeps_SKIP(){
  # Tests Skip messaging output and return code
  rslt=$(utilMsg SKIP 1234 "message");
  assertTrue $?
  assertSame "1234 [SKIP] message" "$rslt";
}

testUtilDeps_DRYRUN(){
  # Tests Dryrun messaging output and return code
  rslt=$(_r_dryrun=true utilMsg SKIP 1234 "message")
  assertTrue $?
  assertSame "1234 [DRYRUN|SKIP] message" "${rslt}";
}

# shellcheck source=/dev/null
source "$(which shunit2)";
