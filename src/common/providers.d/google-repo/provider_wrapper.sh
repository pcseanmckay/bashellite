bashelliteProviderWrapperGoogleRepo() {

  local repo_bin=$(which repo)

  # Check to see if repo has been initialized
  cd ${_r_mirror_tld}/${_n_mirror_repo_name}
  if [ ! -d ".repo" ]; then
    # Need to initialize repo
    utilMsg INFO "$(utilTime)" "Repo not initialized.  Initializing..."
    python3 ${repo_bin} init -u ${_n_repo_url} --mirror
  fi

  # Repo has been initialized, now ready to sync
  utilMsg INFO "$(utilTime)" "Proceeding with sync of repo (${_n_repo_name})..."
  # If dryrun is true, perform dryrun
  if [[ ${_r_dryrun} ]]; then
    utilMsg INFO "$(utilTime)" "Sync of repo (${_n_repo_name}) completed without error..."
  # If dryrun is not true, perform real run
  else
    python3 ${repo_bin} sync;
    if [[ "${?}" == "0" ]]; then
      utilMsg INFO "$(utilTime)" "Sync of repo (${_n_repo_name}) completed without error...";
    else
      utilMsg WARN "$(utilTime)" "Sync of repo (${_n_repo_name}) did NOT complete without error...";
    fi
  fi

}
