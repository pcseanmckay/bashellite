#!/usr/bin/env bash

main() {

  local bin_name="apt-mirror";
  local dep_check_failed="false";

  local provider_name="apt-mirror"
  local deps=( "git" "wget" "make" );
  local bins=( "apt-mirror" );
  local bin_check_failed="false";

  for dep in "${deps[@]}"; do
    dnf -y install ${dep};
  done;

  # for dep in \
  #            git \
  #            wget \
  #            pod2man \
  #            ; do
  #   which ${dep} &>/dev/null \
  #   || {
  #        echo "[FAIL] Can not proceed until ${dep} is installed and accessible in path.";
  #        local dep_check_failed="true";
  #      };
  # done;
  # if [[ "${dep_check_failed}" == "true" ]]; then
  #   echo "[FAIL] ${bin_name} provider can not be installed until missing dependencies are installed; exiting." \
  #   && exit 1;
  # fi;

  if [[ ! -s ${bin_dir}/${bin_name} ]]; then
    echo "[WARN] apt-mirror not installed... installing." \
    && rm -fr ${PROVIDERDIR}/apt-mirror/src/ \
    && git clone https://github.com/apt-mirror/apt-mirror.git ${PROVIDERDIR}/apt-mirror/src/ \
    && cd ${PROVIDERDIR}/apt-mirror/src/ \
    && make install;
  fi

  for bin in "${bins[@]}"; do
    which ${bin} &>/dev/null \
    || {
         echo "[WARN] Can not proceed until ${bin} is installed and accessible in path.";
         local bin_check_failed="true";
       };
  done;
  if [[ "${bin_check_failed}" == "true" ]]; then
    echo "[FAIL] ${provider_name} provider can not be installed until missing dependencies are installed; exiting.";
    exit 1;
  fi;
  echo "[INFO] ${provider_name} provider successfully installed.";

  # if [[ "$(${bin_dir}/${bin_name} --bad-flag &>/dev/null; echo ${?};)" == "2" ]]; then
  #   echo "[INFO] ${bin_name} provider successfully installed.";
  # else
  #   echo "[FAIL] ${bin_name} provider NOT successfully installed; exiting.";
  #   exit 1;
  # fi
}

main
