#!/usr/bin/env bash

main() {

  local provider_name="bandersnatch"
  local deps=( "python3-pip" );
  local bins=( "pip3" "bandersnatch" );
  local bin_check_failed="false";

  for dep in "${deps[@]}"; do
    dnf -y install ${dep};
  done;

  pip3 install bandersnatch
	  
  for bin in "${bins[@]}"; do
    which ${bin} &>/dev/null \
    || {
         echo "[WARN] Can not proceed until ${bin} is installed and accessible in path.";
         local bin_check_failed="true";
       };
  done;
  if [[ "${bin_check_failed}" == "true" ]]; then
    echo "[FAIL] ${provider_name} provider can not be installed until missing dependencies are installed; exiting.";
    exit 1;
  fi;
  echo "[INFO] ${provider_name} provider successfully installed.";
  
  # local providers_tld="/opt/bashellite/providers.d";

  # for dep in \
  #            pip \
  #            virtualenv \
  #            rm \
  #            ; do
  #   which ${dep} &>/dev/null \
  #   || {
  #        echo "[FAIL] Can not proceed until ${dep} is installed and accessible in path; exiting." \
  #        && exit 1;
  #      };
  # done
  # # If pip and virtualenv are installed, ensure bandersnatch is installed in proper location, and functional.
  # # If bandersnatch is not installed, or broken, blow away the old one, and install a new one.
  # ${providers_tld}/bandersnatch/exec/bin/bandersnatch --help &>/dev/null \
  # || {
  #      echo "[WARN] bandersnatch does NOT appear to be installed, (or it is broken); (re)installing..." \
  #      && rm -fr ${providers_tld}/bandersnatch/exec/ &>/dev/null \
  #      && virtualenv --python=python3.6 ${providers_tld}/bandersnatch/exec/ \
  #      && ${providers_tld}/bandersnatch/exec/bin/pip install bandersnatch;
  #    };
  # # Ensure bandersnatch installed successfully
  # {
  #   ${providers_tld}/bandersnatch/exec/bin/bandersnatch --help &>/dev/null \
  #   && echo "[INFO] bandersnatch installed successfully...";
  # } \
  # || {
  #      echo "[FAIL] bandersnatch was NOT installed successfully; exiting." \
  #      && exit 1;
  #    };
}

main
